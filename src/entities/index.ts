import Class from '../interfaces/Class';
import RequireContext = __WebpackModuleApi.RequireContext;

let context: RequireContext | undefined;

// To prevent 'require.context is not a function' error when using typeorm cli
let exported: Class[] | string[];

try {
    context = require.context('./', true, /^\.\/(?!index\.ts)([\d\w]+\.ts)$/);
} catch (err) {
    console.error(err);
}

if (context) {
    const entities = [];

    for (const key of context.keys()) {
        entities.push((context(key) as { default: Class }).default);
    }

    exported = entities;
} else {
    exported = ['src/entities/**/*.ts'];
}

export default exported;
