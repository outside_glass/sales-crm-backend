import CfnResponse from "../../../interfaces/CfnResponse";
import establishDbConnection from "../../../utils/establishDbConnection";
import migrations from '../../../migrations';
import middy = require("middy");
import {doNotWaitForEmptyEventLoop} from "middy/middlewares";
import cfnCustomResourceWrapper from "../../../middlewares/CustomResourceWrapper";

async function runMigrations(): Promise<CfnResponse> {
    try {
        const connection = await establishDbConnection({
            migrations,
        });

        await connection.runMigrations();

        return {
            status: 'SUCCESS',
        };
    } catch (err) {
        console.error(err.message);

        return {
            status: 'FAILED',
        };
    }
}

export default middy(runMigrations)
    .use(doNotWaitForEmptyEventLoop())
    .use(cfnCustomResourceWrapper());
