export default interface Class {
    new(...args: unknown[]): unknown;
}
