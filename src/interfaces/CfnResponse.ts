export default interface CfnResponse {
    status: 'SUCCESS' | 'FAILED';
    reason?: string;
    data?: object;
}
