import { Connection, ConnectionOptions, createConnection, getConnection, getConnectionManager } from 'typeorm';

const ormconfig = require('../../ormconfig');

export default async function establishDbConnection(options?: Partial<ConnectionOptions>) {
    const manager = getConnectionManager();

    let connection: Connection;

    if (manager.has(ormconfig.name)) {
        connection = getConnection(ormconfig.name);
    } else {
        connection = await createConnection({
            ...ormconfig,
            ...options,
        });
    }

    return connection;
}
